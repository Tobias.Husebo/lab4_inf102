package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        return findMedian(listCopy, 0, listCopy.size()-1, listCopy.size()/2);
    }

    
    private <T extends Comparable<T>> T findMedian(List<T> list, int lo, int hi, int median) {
        // Calling the sortAndCheck algorithm, comparing every elem from lo index to hi index
        // against a chosen pivot element against another elem in the list:
        int check = sortAndCheck(list, lo, hi);

        //if check is returned as same value as median, chosen pivot was median
        if (check == median) {
            return list.get(median);
        }
        else if (check < median) {
            // if check is less than median, update lo, so that lesser items than piv is not iterated through again
            // lo functions as a counter towards median:
            return findMedian(list, check+1, hi, median);
        }
        else {
            //if check was over the middle, update hi, so that higher items than piv is not iterated through again
            return findMedian(list, lo, check-1, median);
        }
    }


    private <T extends Comparable<T>> int sortAndCheck(List<T> list, int lo, int hi) {
        //Checking element in the list:

        //
        // Other solution for choosing pivot, slower:
        // Random rand = new Random();
        // int piv = rand.nextInt(hi+1);
        // (switch hi with piv in compElem param)
        //

        T compElem = list.get(hi); 
        int pivotLoc = lo;

        for (int i = lo; i < hi; i++){
            // Moving all elems < compElem behind pivot location
            if (list.get(i).compareTo(compElem) == -1) {
                T temp = list.get(i);
                list.set(i, list.get(pivotLoc));
                list.set(pivotLoc, temp);
                pivotLoc++;
            }
        }

        //After iterating from lo to hi, move our compElem to its new position
        T temp = list.get(hi);
        list.set(hi, list.get(pivotLoc));
        list.set(pivotLoc, temp);

        //At last returning pivot location, if higher than median, update hi, else update lo:
        return pivotLoc;
    }
}






























/* 
 *     private <T extends Comparable<T>> T findMedian(List<T> list, int lo, int hi, int median){                        //K 
        int partition = partition(list, lo, hi);

        if (partition == median) {
            return list.get(partition);
        } 
        else if (partition < median) {
            return findMedian(list, partition+1, hi, median);
        } 
        else {
            return findMedian(list, lo, partition-1, median);
        }
    }

    private <T extends Comparable<T>> int partition(List<T> list, int lo, int hi) {
        T elemToCompare = list.get(hi);
        ArrayList<T> less = new ArrayList<>();
        ArrayList<T> greaterOrEqual = new ArrayList<>();


        for (int i=lo; i<hi; i++) {
            if ( list.get(i).compareTo(elemToCompare) < 0 ) {
                less.add(list.get(i));
            } else {
                greaterOrEqual.add(list.get(i));
            }
        }


        
        return less.size()+lo;
    }
 */