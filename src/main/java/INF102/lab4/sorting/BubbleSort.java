package INF102.lab4.sorting;

import java.util.Collections;
import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {      // O(n^2)
        for (int i = 0; i<list.size(); i++) {
            boolean noSwaps = true;
            for (int j = 1; j<list.size()-i; j++){
                int compare = list.get(j-1).compareTo(list.get(j));
                if (compare == 1){
                    Collections.swap(list, j, j-1);
                    noSwaps = false;
                }
            }
            if (noSwaps == true){
                break;
            }
        }
    }


    /*   WORSE SOLUTION: (TOO SLOW)
       for (int i = 0; i<list.size(); i++) {
            for (int j = 0; j<list.size()-1; j++) {
                T elem1 = list.get(j);
                T elem2 = list.get(j+1);
                int check = elem1.compareTo(elem2);
                if (check == 1) {
                    list.set(j+1, elem1);
                    list.set(j, elem2);
                } 
            }
        }
    */




    
}
